﻿namespace TestSMTPClient
{
	partial class FrmTestSmtpClient
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtFromAddress = new System.Windows.Forms.TextBox();
			this.lblFrom = new System.Windows.Forms.Label();
			this.lblTo = new System.Windows.Forms.Label();
			this.txtToAddress = new System.Windows.Forms.TextBox();
			this.lblServer = new System.Windows.Forms.Label();
			this.txtSmtpServer = new System.Windows.Forms.TextBox();
			this.btnTest = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtFromAddress
			// 
			this.txtFromAddress.Location = new System.Drawing.Point(85, 12);
			this.txtFromAddress.Name = "txtFromAddress";
			this.txtFromAddress.Size = new System.Drawing.Size(259, 20);
			this.txtFromAddress.TabIndex = 0;
			this.txtFromAddress.Text = "RetailIntranetSystem@Clarks.Com";
			// 
			// lblFrom
			// 
			this.lblFrom.AutoSize = true;
			this.lblFrom.Location = new System.Drawing.Point(8, 15);
			this.lblFrom.Name = "lblFrom";
			this.lblFrom.Size = new System.Drawing.Size(71, 13);
			this.lblFrom.TabIndex = 1;
			this.lblFrom.Text = "From Address";
			// 
			// lblTo
			// 
			this.lblTo.AutoSize = true;
			this.lblTo.Location = new System.Drawing.Point(18, 41);
			this.lblTo.Name = "lblTo";
			this.lblTo.Size = new System.Drawing.Size(61, 13);
			this.lblTo.TabIndex = 3;
			this.lblTo.Text = "To Address";
			// 
			// txtToAddress
			// 
			this.txtToAddress.Location = new System.Drawing.Point(85, 38);
			this.txtToAddress.Name = "txtToAddress";
			this.txtToAddress.Size = new System.Drawing.Size(259, 20);
			this.txtToAddress.TabIndex = 2;
			this.txtToAddress.Text = "graham.langridge@clarks.com";
			// 
			// lblServer
			// 
			this.lblServer.AutoSize = true;
			this.lblServer.Location = new System.Drawing.Point(8, 67);
			this.lblServer.Name = "lblServer";
			this.lblServer.Size = new System.Drawing.Size(71, 13);
			this.lblServer.TabIndex = 5;
			this.lblServer.Text = "SMTP Server";
			// 
			// txtSmtpServer
			// 
			this.txtSmtpServer.Location = new System.Drawing.Point(85, 64);
			this.txtSmtpServer.Name = "txtSmtpServer";
			this.txtSmtpServer.Size = new System.Drawing.Size(259, 20);
			this.txtSmtpServer.TabIndex = 4;
			this.txtSmtpServer.Text = "smtp.clarks.com";
			this.txtSmtpServer.TextChanged += new System.EventHandler(this.txtSmtpServer_TextChanged);
			// 
			// btnTest
			// 
			this.btnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTest.Location = new System.Drawing.Point(21, 108);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(317, 35);
			this.btnTest.TabIndex = 6;
			this.btnTest.Text = "Send Test Email";
			this.btnTest.UseVisualStyleBackColor = true;
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// FrmTestSmtpClient
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 147);
			this.Controls.Add(this.btnTest);
			this.Controls.Add(this.lblServer);
			this.Controls.Add(this.txtSmtpServer);
			this.Controls.Add(this.lblTo);
			this.Controls.Add(this.txtToAddress);
			this.Controls.Add(this.lblFrom);
			this.Controls.Add(this.txtFromAddress);
			this.Name = "FrmTestSmtpClient";
			this.Text = "Test .NET SMTP Client";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtFromAddress;
		private System.Windows.Forms.Label lblFrom;
		private System.Windows.Forms.Label lblTo;
		private System.Windows.Forms.TextBox txtToAddress;
		private System.Windows.Forms.Label lblServer;
		private System.Windows.Forms.TextBox txtSmtpServer;
		private System.Windows.Forms.Button btnTest;
	}
}

